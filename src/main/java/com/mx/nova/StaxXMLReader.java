package com.mx.nova;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.mx.nova.exception.GenericException;
import com.mx.nova.modelo.Field;
import com.mx.nova.modelo.LayoutsInfoGeneralArchivo;
import com.mx.nova.modelo.Line;
import com.mx.nova.servicios.PeticionConsultaLayoutEntrada;

public class StaxXMLReader {

	public static void parseXML(String filePath, LayoutsInfoGeneralArchivo layoutsInfoGeneralArchivo) {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		Map<String, Object> mapList = new LinkedHashMap<>();

		List<Field> fieldsHeader = new ArrayList<>();
		List<Field> fieldsBody = new ArrayList<>();
		List<Field> fieldsFooter = new ArrayList<>();

		for (Line linea : layoutsInfoGeneralArchivo.getDefinition().getHeader().getLines()) {
			for (Field field : linea.getFields()) {
				if (!field.getPath().isEmpty()) {
					fieldsHeader.add(field);
				}
			}
		}

		for (Line linea : layoutsInfoGeneralArchivo.getDefinition().getBody().getLines()) {
			for (Field field : linea.getFields()) {
				if (!field.getPath().isEmpty()) {
					fieldsBody.add(field);
				}
			}
		}

		for (Line linea : layoutsInfoGeneralArchivo.getDefinition().getFooter().getLines()) {
			for (Field field : linea.getFields()) {
				if (!field.getPath().isEmpty()) {
					fieldsFooter.add(field);
				}
			}
		}

		Map<String, String> mapBody = new LinkedHashMap<>();
		Map<String, String> mapHeader = new LinkedHashMap<>();
		Map<String, String> mapFooter = new LinkedHashMap<>();

		LocalTime l1 = LocalTime.now();

		int contador = 0;
		int contadorHeader = 0;
		int contadorBody = 0;
		int contadorFooter = 0;
		int nodePosition = 0;
		try {
			String[] cmd = { "/bin/bash", "-c", "cat ".concat(filePath).concat(" | tr -d \"\\t\\n\\r\" ") };
			Process pb = Runtime.getRuntime().exec(cmd);
			InputStream inputStream = pb.getInputStream();

			XMLStreamReader streamReader = factory.createXMLStreamReader(inputStream);
			StringBuilder sb = new StringBuilder();
			while (streamReader.hasNext()) {
				switch (streamReader.next()) {
				case XMLStreamReader.START_ELEMENT:
					sb.append(sb.toString().isEmpty() ? streamReader.getLocalName()
							: ".".concat(streamReader.getLocalName()));

					break;
				case XMLStreamReader.END_ELEMENT:
					sb.delete(
							(sb.toString().contains(".") ? sb.lastIndexOf(".".concat(streamReader.getLocalName())) : 0),
							sb.length());
					if (streamReader.getLocalName().equals("PmtInf")) {
						if (contador <= 1) {
							mapList.put(String.valueOf(++contador), mapHeader);
						}
						mapList.put(String.valueOf(++contador), mapBody);
						mapBody = new LinkedHashMap<>();
						contadorBody = 0;
						nodePosition = 0;
					}

					break;
				case XMLStreamReader.CHARACTERS:

					if (contadorHeader < fieldsHeader.size()) {
						for (Field field : fieldsHeader) {
							if (field.getPath().equals(sb.toString())) {
								mapHeader.put(field.getName(), streamReader.getText());
								++contadorHeader;
							}
						}
					}

					if (contadorBody < fieldsBody.size()) {
						for (Field field : fieldsBody) {
							if (field.getPath().indexOf("[") < 0) {
								if (sb.toString().contains(field.getPath())) {
									mapBody.put(field.getName(), streamReader.getText());
								}

							} else {
								if (sb.toString()
										.contains(field.getPath().substring(0, field.getPath().indexOf("[")))) {
									if (nodePosition == Integer.parseInt(field.getPath().substring(
											field.getPath().indexOf("[") + 1, field.getPath().indexOf("]")))) {
										mapBody.put(field.getName(), streamReader.getText());
										contadorBody++;
										nodePosition++;
										break;
									}
								}
							}
						}
					}

					if (contadorFooter < fieldsFooter.size()) {
						for (Field field : fieldsFooter) {
							if (field.getPath().equals(sb.toString())) {
								mapFooter.put(field.getName(), streamReader.getText());
								++contadorFooter;
							}
						}
					}
					streamReader.next();
					sb.delete(
							(sb.toString().contains(".") ? sb.lastIndexOf(".".concat(streamReader.getLocalName())) : 0),
							sb.length());

					break;
				default:
					break;
				}
			}
			mapList.put(String.valueOf(++contador), mapFooter);
		} catch (XMLStreamException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		LocalTime l2 = LocalTime.now();
		mapList.forEach((k, v) -> {
			System.out.println(k + ": " + v);
		});

		System.out.println(Duration.between(l1, l2));
	}

	@SuppressWarnings("unused")
	private static String compareStrings(List<Field> fields) {
		StringBuilder sb = new StringBuilder();
		String aux = "";
		String[] splitField1 = fields.get(0).getPath().split("\\.");

		for (int j = 0; j < fields.size(); j++) {
			sb.delete(0, sb.length());
			for (int k = 0; k < splitField1.length; k++) {
				if (fields.get(j).getPath().contains(splitField1[k])) {
					sb.append(splitField1[k]).append(".");
				} else
					break;
			}

			aux = (aux.isEmpty() || aux.length() > sb.toString().length())
					? sb.toString().substring(0, sb.toString().length() - 1)
					: aux;

		}
		aux = aux.substring(aux.lastIndexOf(".") + 1);
		for (Field field : fields) {
			field.setPath(field.getPath().split(aux.concat("."))[1]);
		}

		return aux;

	}

	public static LayoutsInfoGeneralArchivo leerLayout() {
		PeticionConsultaLayoutEntrada peticionConsultaLayoutEntrada = new PeticionConsultaLayoutEntrada();
		LayoutsInfoGeneralArchivo layoutsInfoGeneralArchivo = null;
		try {
			layoutsInfoGeneralArchivo = peticionConsultaLayoutEntrada.consultaLayoutArchivoGeneral();
			return layoutsInfoGeneralArchivo;
		} catch (GenericException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return layoutsInfoGeneralArchivo;
	}

}