package com.mx.nova.exception;

public class GenericException extends Exception {

	private static final long serialVersionUID = -8743394862762904685L;

	public GenericException(String message) {
		super(message);
	}

	/**
	 * Excepcion generica para mensaje y Throwable
	 * 
	 * @param message
	 * @param throwable
	 */
	public GenericException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
