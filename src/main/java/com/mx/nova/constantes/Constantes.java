package com.mx.nova.constantes;

public class Constantes {

	public static final String DEFINICION = "definicion";
	public static final String HEADER = "H";
	public static final String BODY = "B";
	public static final String FOOTER = "F";
	public static final String ID = "_id";

	public static final String BANCO = "bank";
	public static final String CITIBANAMEX = "citibanamex";

	// Logs de Errores
	public static final String LOG_ERROR_MAPEO_DEFINICION = "Error al mapear la Definicion";
	// public static final String LOG_ERROR_EN_CONSULTAR_MONGO = "Error al obtener
	// informacion de Mongo";
	public static final String LOG_ERROR_OBTENER_INFORMACION_ARCHIVO_ENTRADA = "Error obtener informacion de archivo Entrada";
	// public static final String LOG_ERROR_GENERAR_MAPA_ARCHIVO_ENTRADA = "Error al
	// generar Archivo Entrada";
	public static final String LOG_ERROR_CARGAR_ARCHIVO_ENTRADA = "Error al cargar Archivo Entrada";
	public static final String LOG_ERROR_CONVERTIR_ARCHIVO = "Error al convertir el Archivo";
	public static final String LOG_ERROR_GENERAR_PARAMETRO_LONGITUD = "Error al genera Patron de Longitud";
	// Logs de Errores en Procesa Convertidor
	public static final String LOG_ERROR_CONSULTAR_LAYOUT = "Error al consultar Layout del Archivo";
	public static final String LOG_ERROR_CARGAR_ARCHIVO = "Error al cargar el Archivo de entrada";
	public static final String LOG_ERROR_PROCESAR_LAYOUT = "Error al procesar el layout de Entrada";
	public static final String LOG_ERROR_CONSULTAR_LAYOUT_ARCHIVO = "Error al consultar el Layout de Archivo";
	public static final String LOG_ERROR_CONSULTAR_LAYOUT_MAPEO = "Error al consultar el Layout de Archivo Mapeo";
	public static final String LOG_ERROR_GENERAR_ARCHIVO_HEADERS = "Error al generar el Archivo de salida de Headers";
	public static final String LOG_ERROR_GENERAR_ARCHIVO_BODY = "Error al generar el Archivo de salida de Body";
	public static final String LOG_ERROR_GENERAR_ARCHIVO_FOOTER = "Error al generar el Archivo de salida de Footer";

	public static final String LOG_ERROR_PROCESAR_ARCHIVO_HEADERS = "Error al procesar el Archivo de salida de Headers";
	public static final String LOG_ERROR_PROCESAR_ARCHIVO_BODY = "Error al procesar el Archivo de salida de Body";
	public static final String LOG_ERROR_PROCESAR_ARCHIVO_FOOTER = "Error al procesar el Archivo de salida de Footer";
	public static final String LOG_ERROR_PROCESAR_ARCHIVO_HEADER_FOOTER = "Error al procesar el Archivo de salida de Header-Footer";

	public static final String LOG_ERROR_VALIDA_CONSTANTES = "Error al validar constantes del Archivo Salida";
	public static final String LOG_ERROR_VALIDA_CAMPO_MAPEO = "Error al validar campo Mapeo del Archivo Salida";
	public static final String LOG_ERROR_VALIDA_FUNCION = "Error al validar funcion del Archivo Salida";
	public static final String LOG_ERROR_VALIDA_PARAMETROS_ENTRADA = "Error al validar parametros entrada del Archivo Salida";

	public static final String ERROR_SUBIR_ARCHIVO = "Error al subir el archivo";
	public static final String ERROR_GENERAR_RESPUESTA_SERVICIO = "Error al generar la respuesta del servicio";
	public static final String ERROR_FORMATO_INFORMACION_RESPUESTA = "Error al extraer la informacion de la Respuesta";

	// logs Proceso Convercion Formatos
	public static final String LOG_ERROR_PROCESO_CONVERTIR_FORMATO_TXT = "Error enproceso de convercion Archivo formato txt";
	// public static final String LOG_ERROR_LECTURA_ARCHIVO_ZIP= "Error leer el
	// archivo zip";

	public static final String LOG_ERROR_PROCESO_DE_SALIDA_ARCHIVO = "Error al generar el proceso de salida del Archivo";

	// Constantes para envio de Exceptiones en parametros
	public static final String LOG_ERROR_EN_CONSULTAR_MONGO = "Error al obtener informacion de Mongo";
	public static final String LOG_ERROR_ARCHIVO_NO_ENVIADO = "Error archivo requerido";

	public static final String LOG_ERROR_LECTURA_ARCHIVO_ZIP = "Error leer el archivo zip";
	public static final String LOG_ERROR_OBTENER_LINEAS = "Error Obtener la deficion de lineas";

	public static final String LOG_ERROR_GENERAR_MAPA_ARCHIVO_ENTRADA = "Error al generar Archivo Entrada";

	public static final String LOG_ERROR_EN_CONSULTAR_LAYOUTS = "Error al consultar layouts";

	public static final String LOG_ERROR_GENERAR_ARCHIVO_SALIDA = "Error en generar archivo de Salida";
	public static final String LOG_ERROR_CREAR_ARCHIVO_ZIP = "Error crear el archivo zip";
	public static final String LOG_ERROR_AL_CREAR_FORMATO_BASE64 = "Error crear el archivo en base64";
	public static final String LOG_ERROR_AL_CREAR_ARCHIVO_ENTRADA = "Error crear archivo de entrada";

	public static final String LOG_ERROR_AL_OBTENER_FUNCIONES = "Error al obtener las funciones de layout";

}
