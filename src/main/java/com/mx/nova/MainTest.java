package com.mx.nova;

import java.io.IOException;

import com.mx.nova.modelo.LayoutsInfoGeneralArchivo;

public class MainTest {

	public static void main(String[] args) throws IOException {
		String path = System.getenv("NOVA_PATH").concat("100000.xml");
		LayoutsInfoGeneralArchivo layoutsInfoGeneralArchivo = StaxXMLReader.leerLayout();
		StaxXMLReader.parseXML(path, layoutsInfoGeneralArchivo);
	}
}
