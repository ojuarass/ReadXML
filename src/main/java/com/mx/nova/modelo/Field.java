
package com.mx.nova.modelo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "viewName", "name", "path", "type", "length", "constant", "alignment", "fill", "starts", "format",
		"subtype" })
public class Field implements Serializable {

	private static final long serialVersionUID = 8934757400822675594L;

	@JsonProperty("viewName")
	private String viewName;
	@JsonProperty("name")
	private String name;
	@JsonProperty("path")
	private String path;
	@JsonProperty("type")
	private String type;
	@JsonProperty("length")
	private String length;
	@JsonProperty("constant")
	private String constant;
	@JsonProperty("alignment")
	private String alignment;
	@JsonProperty("fill")
	private String fill;
	@JsonProperty("starts")
	private String starts;
	@JsonProperty("format")
	private String format;
	@JsonProperty("subtype")
	private String subtype;

	/**
	 * No args constructor for use in serialization
	 */
	public Field() {
	}

	/**
	 * @param viewName
	 * @param name
	 * @param path
	 * @param type
	 * @param length
	 * @param constant
	 * @param alignment
	 * @param fill
	 * @param starts
	 * @param format
	 * @param subtype
	 */
	public Field(String viewName, String name, String path, String type, String length, String constant,
			String alignment, String fill, String starts, String format, String subtype) {
		super();
		this.viewName = viewName;
		this.name = name;
		this.path = path;
		this.type = type;
		this.length = length;
		this.constant = constant;
		this.alignment = alignment;
		this.fill = fill;
		this.starts = starts;
		this.format = format;
		this.subtype = subtype;
	}

	@JsonProperty("viewName")
	public String getViewName() {
		return viewName;
	}

	@JsonProperty("viewName")
	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("path")
	public String getPath() {
		return path;
	}

	@JsonProperty("path")
	public void setPath(String path) {
		this.path = path;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("length")
	public String getLength() {
		return length;
	}

	@JsonProperty("length")
	public void setLength(String length) {
		this.length = length;
	}

	@JsonProperty("constant")
	public String getConstant() {
		return constant;
	}

	@JsonProperty("constant")
	public void setConstant(String constant) {
		this.constant = constant;
	}

	@JsonProperty("alignment")
	public String getAlignment() {
		return alignment;
	}

	@JsonProperty("alignment")
	public void setAlignment(String alignment) {
		this.alignment = alignment;
	}

	@JsonProperty("fill")
	public String getFill() {
		return fill;
	}

	@JsonProperty("fill")
	public void setFill(String fill) {
		this.fill = fill;
	}

	@JsonProperty("starts")
	public String getStarts() {
		return starts;
	}

	@JsonProperty("starts")
	public void setStarts(String starts) {
		this.starts = starts;
	}

	@JsonProperty("format")
	public String getFormat() {
		return format;
	}

	@JsonProperty("format")
	public void setFormat(String format) {
		this.format = format;
	}

	@JsonProperty("subtype")
	public String getSubtype() {
		return subtype;
	}

	@JsonProperty("subtype")
	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Field [viewName=");
		builder.append(viewName);
		builder.append(", name=");
		builder.append(name);
		builder.append(", path=");
		builder.append(path);
		builder.append(", type=");
		builder.append(type);
		builder.append(", length=");
		builder.append(length);
		builder.append(", constant=");
		builder.append(constant);
		builder.append(", alignment=");
		builder.append(alignment);
		builder.append(", fill=");
		builder.append(fill);
		builder.append(", starts=");
		builder.append(starts);
		builder.append(", format=");
		builder.append(format);
		builder.append(", subtype=");
		builder.append(subtype);
		builder.append("]");
		return builder.toString();
	}

}
