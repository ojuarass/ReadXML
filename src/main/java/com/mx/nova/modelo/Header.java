
package com.mx.nova.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "lines" })
public class Header implements Serializable {

	private static final long serialVersionUID = -8890972081599563030L;

	@JsonProperty("lines")
	private List<Line> lines = new ArrayList<>();

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Header() {
	}

	/**
	 * 
	 * @param lines
	 */
	public Header(List<Line> lines) {
		super();
		this.lines = lines;
	}

	@JsonProperty("lines")
	public List<Line> getLines() {
		return lines;
	}

	@JsonProperty("lines")
	public void setLines(List<Line> lines) {
		this.lines = lines;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Header [lines=");
		builder.append(lines);
		builder.append("]");
		return builder.toString();
	}

}
