
package com.mx.nova.modelo;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "H", "B", "F" })
public class Definition implements Serializable {

	private static final long serialVersionUID = 7921636210210974471L;

	@JsonProperty("H")
	private Header header;
	@JsonProperty("B")
	private Body body;
	@JsonProperty("F")
	private Footer footer;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Definition() {
	}

	/**
	 * @param footer
	 * @param body
	 * @param header
	 */
	public Definition(Header header, Body body, Footer footer) {
		super();
		this.header = header;
		this.body = body;
		this.footer = footer;
	}

	@JsonProperty("H")
	public Header getHeader() {
		return header;
	}

	@JsonProperty("H")
	public void setH(Header header) {
		this.header = header;
	}

	@JsonProperty("B")
	public Body getBody() {
		return body;
	}

	@JsonProperty("B")
	public void setB(Body body) {
		this.body = body;
	}

	@JsonProperty("F")
	public Footer getFooter() {
		return footer;
	}

	@JsonProperty("F")
	public void setF(Footer footer) {
		this.footer = footer;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Definition [header=");
		builder.append(header);
		builder.append(", body=");
		builder.append(body);
		builder.append(", footer=");
		builder.append(footer);
		builder.append("]");
		return builder.toString();
	}

}
