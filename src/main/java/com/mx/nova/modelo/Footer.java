
package com.mx.nova.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "lines" })
public class Footer implements Serializable {

	private static final long serialVersionUID = -6139140860262071831L;

	@JsonProperty("lines")
	private List<Line> lines = new ArrayList<>();

	/**
	 * No args constructor for use in serialization
	 */
	public Footer() {

	}

	/**
	 * @param lines
	 */
	public Footer(List<Line> lines) {
		super();
		this.lines = lines;
	}

	@JsonProperty("lines")
	public List<Line> getLines() {
		return lines;
	}

	@JsonProperty("lines")
	public void setLines(List<Line> lines) {
		this.lines = lines;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Footer [lines=");
		builder.append(lines);
		builder.append("]");
		return builder.toString();
	}

}
