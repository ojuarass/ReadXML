package com.mx.nova.util;

import com.fasterxml.jackson.databind.ObjectMapper;

public enum Maper {

	INSTANCE;

	private final ObjectMapper mapper = new ObjectMapper();

	public ObjectMapper getObjectMapper() {
		return mapper;
	}
}