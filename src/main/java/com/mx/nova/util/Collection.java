package com.mx.nova.util;

public enum Collection {

	COLECCION_MONGO_LAYOUTS("layouts.json");

	private String path;

	private Collection(String path) {
		this.path = path;
	}

	public String getPath() {
		return path;
	}
}
